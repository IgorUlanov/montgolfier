#ifndef MAIN_H
#define MAIN_H
/*!
@file main.h
@brief Описание программы Montgolfier.
*/

/*!
\mainpage

\section objective Задача - по задаваемым пользователем параметрам пересчитывать и выводить технические характеристики воздушого шара.

Параметры, которые задаются пользователем:
 - параметры, которые определяют форму и размер воздушного шара:
	- диаметр шара (в см).
	- диаметр выхода (в см).
	- угол раскрыва (в градусах).
 - параметры, которые определяют летные качества воздушного шара:
	- температура окружающего воздуха (в градусах цельсия).
	- разница температур между окружающей температурой и температурой воздуха в шаре (в градусах цельсия).
	- плотность материала (в г/м2).
 - параметры, которые определяют размеры выкройки и таблицы с данными выкройки:
	- количество частей выкройки (лепестков шара).
	- шаг отсчетов (в см). Шаг с которым будут обсчитываться точки для построения выкройки.

Параметры, которые обсчитывает программа и выводит на экран:
 - длина выкройки (в см).
 - ширина выкройки (в см).
 - площадь оболчки (в м2).
 - объем шара (в м3).
 - вес оболочки шара (в кг).
 - вес воздуха в шаре (в кг).
 - подъемная сила шара (в кг).
 - количество отсчетов при построении таблицы с данными выкройки.

Также программа рисует внешний вид шара, чтобы можно было оценить форму и размер будущего шара.

\section general Общая схема программы:
Класс Montgolfier наследуемый от QMainWindow, является основным классом обеспечивающим функционирование главного окна программы.
 - обеспечивает доставку параметров задаваемых пользователем классу Machine, и вывод в главное окно программы результатов расчетов, которые получает также от класса Machine.
 - обеспечивает связь между элементами управления, настроенными в проекте формы главного окна montgolfier.ui.
 - осуществляет переключения языка, встроенными средствами класса QMainWindow.
 - сохранение на принтер или в файл результатов расчета выкройки, с помощью класса Print.

Класс Machine выполняет функции распорядителя, который определяет действия при изменении тех или иных параметров пользователем. В этом классе описан порядок обращения к функциям классов Up, Down, Pyr и Out, которые выполняют непосредственные расчеты частей шара. Шар условно разбит на 4 составные части. Верхняя  полусфера шара описана в классе Up. Часть нижней полусферы шара - в классе Down, которая ограничена пирамидальной частью, примыкающей к шару снизу и которая описана в классе - Pyr. И класс Out, который описывает несуществующюю часть пирамиды, т.е. ту которая расположена "ниже" выходного отверстия шара. Класс Machine, также обеспечивает заполнение структуры Common, в которой хранятся параметры описывающие весь шар.

В классах Up, Down, Pyr и Out - сохраняются параметры индивидуальный для каждой части шара, а также реализованы функции, которые высчитывают длину выкройки, площадь и объем этих частей. Все эти классы являются наследниками класса Base.

Класс Paint рисует на поверхности widget воздушный шар согласно заданным параметрам. Также считывает из файла man.tif человечек, которого выводит также на widget для оценки размера будущего шара.

Класс Print обеспечивает вывод таблицы с данными выкройки для шара. Вывод осуществляется как на принтер так и в файл формата pdf. Имя файла создается по следующему шаблону - d100_a60_o12.pdf. Где число после буквы "d" - диаметр шара в см, число после "a" - угол раскрыва, после "o" - диаметр выходного отверстия.

В структуре Common хранятся данные общие для всего шара.

В файле message.h находятся строки необходимые для перевода, которыми заполняется странички с таблицей выкройки.

@author Igor Ulanov
*/

#endif // MAIN_H
