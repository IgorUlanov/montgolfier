.TH Montgolfier 1 "декабрь 2010" "Montgolfier 2.0"
.SH "НАИМЕНОВАНИЕ"
Montgolfier \- программа проектирования воздушного шара на солнечной тяге.
.SH "ОПИСАНИЕ"
Что такое Montgolfier?
.PP
Montgolfier - программа, которая помогает упростить этап проектирования воздушного шара на солнечной тяге. Программа позволяет оценить количество материала, подъемную силу, размеры заготовок, форму и еще некоторые вещи, которые удобно знать в процессе проектирования.
.P
Программа подсчитывает:
.RS
длину выкройки шара 
.br
ширину выкройки шара 
.br
площадь оболочки шара 
.br
объем шара 
.br
вес оболочки шара 
.br
вес воздуха в шаре 
.br
подъемную силу 
.br
.RE
.P
Форма проектируемого шара и его размер относительно человека среднего роста (1.75м) отображается в левой половине окошка программы.
.P
Параметры определяющие размер и форму воздушного шара:
.RS
диаметр шара
.br
угол раскрыва аппендикса
.br
диаметр горловины аппендикса
.br
количество клиньев шара
.br
.RE
.P
Параметры влияющие только на подъемную силу:
.RS
плотность материала
.br
температура воздуха
.br
разница температур внутри шара и наружи
.br
.RE
.P
Параметры определяющие таблицу с координатами выкройки:
.RS
шаг отсчета по оси Х
.br
количество отсчетов по длине выкройки
.br
.RE
.SH Лицензия
Эта программа распространяется в надежде, что она будет полезной, но БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ; даже без подразумеваемых гарантий КОММЕРЧЕСКОЙ ЦЕННОСТИ или ПРИГОДНОСТИ ДЛЯ КОНКРЕТНОЙ ЦЕЛИ.  Для получения подробных сведений смотрите Универсальную Общественную Лицензию GNU.
.PP
Если вы обнаружили в программе какие-то недостатки или недоработки, пришлите, пожалуйста, ваши замечания по e-mail на адрес:
.IR igor.my.post@mail.ru .
