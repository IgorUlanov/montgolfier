#include "paint.h"
#include "calculator.h"
#include <QPainter>
#include <QPixmap>
#include <QBitmap>
#include "math.h"
#include <QDebug>

extern Common *common;
extern Pyr pyr;
extern Out out;

Paint::Paint(QWidget *parent) : QWidget(parent){
}


void Paint::paintEvent(QPaintEvent *){
	const float w=2.0; // толщина пера
	float r;
	float X=0;
	float Y=0;

	QPainter myGraph(this);
	myGraph.setRenderHint(QPainter::Antialiasing, true);
	QPen myPen=QPen(Qt::lightGray, w, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
	myGraph.setPen(myPen);
	r=common->r; // радиус шара
	float h_ball=r+r*common->sin_A; // высота шаровой части
	float h_pyr=common->cos_A*(pyr.Get_L()-out.Get_L()); // высота пирамиды
	float k;
	if ((h_ball+h_pyr)>stature)	k=(h_ball+h_pyr)/(height()-w); // шар вписываем в y пикселей
	else k=stature/(height()-w); // шар вписываем пропорционально отношению к stature
	if (((r*2)/k)>(width()-w*2)) k=(r*2)/(width()-w*2); // если диаметр шара больше ширины области
	r=r/k; // радиус шара на рисунке
	h_ball=h_ball/k; // высота шаровой части
	h_pyr=h_pyr/k; // высота пирамиды
	float r_out=common->r_out/k; // радиус выхода
	float bias_y=(height()-h_ball-h_pyr-w)/2+w;
	float bias_x=width()/2-r;
	// рисунок шара
	QPainterPath myPath;
	Y=bias_y+h_ball+h_pyr;
	X=r+r_out+bias_x;
	myPath.moveTo(X,Y); // правая точка выхода
	myPath.arcTo(bias_x,bias_y,r+r,r+r,(0-common->degree/2),(common->degree+180));
	X=r-r_out+bias_x;
	myPath.lineTo(X,Y); // левая точка выхода
	X=r+r_out+bias_x;
	myPath.lineTo(X,Y); // правая точка выхода
	QRadialGradient radialGrad(bias_x+r,bias_y+r,r);
	radialGrad.setColorAt(0,this->palette().color(QPalette::Active,QPalette::Window));
	radialGrad.setColorAt(1,this->palette().color(QPalette::Disabled,QPalette::Text));
	myGraph.fillPath(myPath,radialGrad);
	// рисунок шара сверху
	QPainterPath *upPath = new QPainterPath();
	upPath->moveTo(bias_x+r,bias_y);
	float a=(pi*2)/common->n;
	float angle=a;
	bias_x+=r;
	bias_y+=r;
	for (int i=0;i<common->n;i++){
		X=r*sin(angle);
		Y=r*cos(angle);
		upPath->lineTo(bias_x+X,bias_y-Y);
		angle+=a;
	}
	upPath->closeSubpath();
	myGraph.drawPath(*upPath);
	// рисуем человечка
	QPixmap *man=new QPixmap(":/my/img/man.png","PNG",Qt::DiffuseDither | Qt::DiffuseAlphaDither);
	if (!man->isNull()){
		// вывод человечка, если есть файл
		float x=stature/k;
        Y=h_ball+h_pyr;
        if (Y<x) Y=height()/2-x/2;
        else Y-=x+r-bias_y;
        myGraph.drawPixmap(bias_x,Y,man->scaled(x,x,Qt::KeepAspectRatio,Qt::SmoothTransformation));
	}
}
