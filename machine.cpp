#include "math.h"
#include "machine.h"
#include "calculator.h"

extern Common *common;
extern Up up;
extern Down down;
extern Pyr pyr;
extern Out out;

Machine::Machine()
{
}

void Machine::Set_Angle(int d)
{ // запись нового значения угла раскрыва
	common->degree=d; // запись значения угла раскрыва
	common->angle=((double)d*pi)/360; // запись значения угла раскрыва
	common->sin_A=sin(common->angle);
	common->cos_A=cos(common->angle);
}

void Machine::Set_N(int d)
{ // запись количества частей шара
	common->n=d;
}

void Machine::Set_D_out(int d)
{ // запись значения радиуса выхода
	common->r_out=(double)d/200;
}

void Machine::Set_M(int d)
{ // запись веса материала
	common->m=d;
}

void Machine::Change_D(int d)
{ // функция пересчета параметров при изменении диаметра шара
	common->r=(double)d/200; // сохранение нового радиуса
	L=up.Calc_L()+down.Calc_L()+pyr.Calc_L()-out.Calc_L(); // подсчет длины выкройки
	common->w=(2*pi*common->r)/common->n; // ширина заготовки
	S=up.Calc_S()+down.Calc_S()+pyr.Calc_S()-out.Calc_S(); // подсчет площадь выкройки
	V=up.Calc_V()+up.Get_V()-down.Calc_V()+pyr.Calc_V()-out.Calc_V(); // подсчет объем выкройки
	Calc_P(); // пересчет веса шара, воздуха и подъемной силы
}

void Machine::Change_Angle(int d)
{ // функция пересчета параметров при изменении угла раскрыва
	Set_Angle(d);
	L=up.Get_L()+down.Calc_L()+pyr.Calc_L()-out.Calc_L(); // подсчет длины выкройки
	S=up.Get_S()+down.Calc_S()+pyr.Calc_S()-out.Calc_S(); // подсчет площадь выкройки
	V=up.Get_V()+up.Get_V()-down.Calc_V()+pyr.Calc_V()-out.Calc_V(); // подсчет объем выкройки
	Calc_P(); // пересчет веса шара, воздуха и подъемной силы
}

void Machine::Change_D_out(int d)
{ // функция пересчета параметров при изменении диаметра выходного отверстия
	Set_D_out(d); // запись значения радиуса выхода
	L=up.Get_L()+down.Get_L()+pyr.Get_L()-out.Calc_L(); // подсчет длины выкройки
	S=up.Get_S()+down.Get_S()+pyr.Get_S()-out.Calc_S(); // подсчет площадь выкройки
	V=up.Get_V()+up.Get_V()-down.Get_V()+pyr.Get_V()-out.Calc_V(); // подсчет объем выкройки
	Calc_P(); // пересчет веса шара, воздуха и подъемной силы
}

void Machine::Change_N(int d)
{// функция пересчета параметров при изменении количества лепестков выкройки
	Set_N(d); // запись количества частей шара
	common->w=(2*pi*common->r)/d; // ширина заготовки
}

void Machine::Change_T(int t,int d)
{ // функция пересчета параметров при изменении температур
	common->t_air=t; // новая температура воздуха
	common->t_ball=t+d; // новая температура в шаре
	Calc_P();
}

void Machine::Change_M(int d)
{ // функция пересчета параметров при изменении веса материала
	Set_M(d);
	m_ball=(common->m*S)/1000; // вес оболочки
	p=m_out-m_air-m_ball; //подъемная сила шара
}

void Machine::Calc_P(void)
{ // пересчет веса шара, воздуха и подъемной силы
	m_ball=(common->m*S)/1000; // вес оболочки
	m_air=(0.0473*760*9.8*V)/(273+common->t_ball); // вес воздуха в оболочке
	m_out=(0.0473*760*9.8*V)/(273+common->t_air); // вес воздуха снаружи
	p=m_out-m_air-m_ball; //подъемная сила шара
}


double Machine::Get_limit(void)
{ // возврат значения максимального диаметра выходного отверстия
    return (common->r_max*200);
}

double Machine::Get_w(void)
{ // возврат значения ширины выкройки
	return (common->w*100);
}
