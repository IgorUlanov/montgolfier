#include "help.h"
#include <qlayout.h>
#include <qstring.h>
#include <qdesktopservices.h>
#include <qpushbutton.h>
#include <qtextbrowser.h>

HelpBrowser::HelpBrowser(const QString &path, const QString &page, QWidget *parent) : QWidget(parent)
{
setAttribute(Qt::WA_DeleteOnClose);
setAttribute(Qt::WA_GroupLeader);
textBrowser = new QTextBrowser;
QVBoxLayout *mainLayout = new QVBoxLayout;
mainLayout->addWidget(textBrowser);
setLayout(mainLayout);
textBrowser->setSearchPaths(QStringList() << path << ":my/help");
textBrowser->setSource(page);
textBrowser->setOpenLinks(false);
connect(textBrowser, SIGNAL(anchorClicked(const QUrl &)), this, SLOT(updateWindowTitle(const QUrl &)));
}

void HelpBrowser::updateWindowTitle(const QUrl & link)
{
if (link.isRelative())
	{
	textBrowser->setSource(link);
    setWindowTitle(QString("Help: %1").arg(textBrowser->documentTitle()));
    }
else QDesktopServices::openUrl(link);
}

void HelpBrowser::showPage(const QString &page,int x,int y)
{
HelpBrowser *browser = new HelpBrowser(":my/help", page);
browser->resize(x, y);
browser->show();
}
