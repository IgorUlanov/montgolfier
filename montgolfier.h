#ifndef MONTGOLFIER_H
#define MONTGOLFIER_H

#include <QMainWindow>
#include <QtPrintSupport/QPrinter>
#include <QTranslator>
#include <QDir>
#include <QAction>
#include <QVector>
#include <QFileDialog>
#include <QLabel>
#include <QSlider>
#include <QSpinBox>
#include <QStatusBar>
#include <QMenuBar>
#include <QToolBar>
#include "paint.h"

/*!
@file montgolfier.h
@brief Объявление класса Montgolfier - основное окно программы.
*/
/*
Montgolfier - программа расчета воздушного шара.

Copyright (C) 2010 Igor Ulanov

	Это программа является свободным программным обеспечением. Вы можете распространять и/или модифицировать её согласно условиям Стандартной Общественной Лицензии GNU, опубликованной Фондом Свободного Программного Обеспечения, версии 3 или, по Вашему желанию, любой более поздней версии.
Эта программа распространяется в надежде, что она будет полезной, но БЕЗ ВСЯКИХ ГАРАНТИЙ, в том числе подразумеваемых гарантий ТОВАРНОГО СОСТОЯНИЯ ПРИ ПРОДАЖЕ и ГОДНОСТИ ДЛЯ ОПРЕДЕЛЁННОГО ПРИМЕНЕНИЯ. Смотрите Стандартную Общественную Лицензию GNU для получения дополнительной информации.
Вы должны были получить копию Стандартной Общественной Лицензии GNU вместе с программой. В случае её отсутствия, посмотрите <http://www.gnu.org/licenses/gpl.html>.
*/

/*namespace Ui {
    class Montgolfier;
}*/
/**
Класс наследуемый от QMainWindow, является основным классом обеспечивающим функционирование главного окна программы.
 - обеспечивает доставку параметров задаваемых пользователем классу Machine, и вывод в главное окно программы результатов расчетов, которые получает также от класса Machine.
 - обеспечивает связь между элементами управления, настроенными в проекте формы главного окна montgolfier.ui.
 - осуществляет переключения языка, встроенными средствами класса QMainWindow.
 - сохранение на принтер или в файл результатов расчета выкройки, с помощью класса Print.
*/
class Montgolfier : public QMainWindow {
    Q_OBJECT
public:
        /** Конструктор. Выбирается язык и запускается переводчик. Создаются связи между событиями и функциями обработки - connect'ы.
            Заносятся и обсчитываются первоначальные параметры шара, заданные по умолчанию. Считывается "человечек" и рисуется на widget вместе с шаром.*/
    Montgolfier(QWidget *parent = 0);
    ~Montgolfier();
protected:
	/// в этой функции вылавливается событие изменения языка.
   void changeEvent(QEvent *e);
private:
	QWidget *mainWidget;
	/// константа с максимальным количеством шагов на выкройке.
	static const int count_step=140;
	void Refresh_all(void);
	void Refresh_t(void);
    QAction *txtAct;
    QAction *csvAct;
    QAction *pdfAct;
    QAction *helpAct;
	Paint *widget;
	QTranslator apptr;
	QPrinter *printer;
	QDir dir;
	QString path_save;
	QAction* newAct;
	QAction* aboutQtAct;
	QMenu* helpMenu;
	QMenu *menuLanguage;
	QVector<QString> locales;
	QFileDialog* dialog;
	QMap<QWidget*, QString> m_widgetInfo;
	QMenu *fileMenu;
	QAction *saveAct;
	QAction *printAct;
	QAction *exitAct;
	QAction *licenseAct;
	QAction *programAct;
	QAction *aboutAct;

	QLabel *L_label;
	QLabel *W_label;
	QLabel *M_label;
	QLabel *V_label;
	QLabel *S_label;
	QLabel *M_air_label;
	QLabel *P_label;
	QSlider *T_Slider;
	QSpinBox *T_spinBox;
	QSpinBox *Delta_spinBox;
	QSlider *Delta_Slider;
	QSpinBox *D_spinBox;
	QSlider *D_Slider;
	QSpinBox *Angle_spinBox;
	QSlider *Angle_Slider;
	QSpinBox *D_out_spinBox;
	QSlider *D_out_Slider;
	QSpinBox *M_spinBox;
	QSpinBox *Step_spinBox;
	QSpinBox *N_spinBox;
	QSpinBox *C_spinBox;
	QMenuBar *menuBar;
    QStatusBar *statusBar;
	QToolBar *toolBar;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_14;
    QLabel *label_15;
    QLabel *label_16;
    QLabel *label_20;
    QLabel *label_21;
    QLabel *label_30;
    QLabel *label_31;
    QLabel *label_32;
    QLabel *label_34;
    QLabel *label_35;
    QLabel *label_40;
    QLabel *label_41;
    QLabel *label_42;
    QLabel *label_43;

private slots:
	void changeLanguage(void);
	void actionSave_As();
	void actionExit();
	void actionPrint();
    void actionText();
    void actionCSV();
    void actionPDF();
    void actionHelp();
	void actionProgram();
	void actionLicense();
	void actionAbout();
	void Change_D(int);
	void Change_Angle(int);
	void Change_D_out(int);
	void Change_N(int);
	void Change_T(void);
	void Change_M(int);
	void Change_Step(void);
	void Limit(void);
	void Limit_step(void);
    void Delta_spinBox_changed(int);
    void T_spinBox_changed(int);
    void D_spinBox_changed(int);
    void Angle_spinBox_changed(int);
    void D_out_spinBox_changed(int);
};

#endif // MONTGOLFIER_H
