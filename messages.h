#ifndef MESSAGES_H
#define MESSAGES_H
/*!
@file messages.h
@brief Строки используемые программой, подлежащие переводу.
*/

/*
Montgolfier - программа расчета воздушного шара.

Copyright (C) 2010 Igor Ulanov

	Это программа является свободным программным обеспечением. Вы можете распространять и/или модифицировать её согласно условиям Стандартной Общественной Лицензии GNU, опубликованной Фондом Свободного Программного Обеспечения, версии 3 или, по Вашему желанию, любой более поздней версии.
Эта программа распространяется в надежде, что она будет полезной, но БЕЗ ВСЯКИХ ГАРАНТИЙ, в том числе подразумеваемых гарантий ТОВАРНОГО СОСТОЯНИЯ ПРИ ПРОДАЖЕ и ГОДНОСТИ ДЛЯ ОПРЕДЕЛЁННОГО ПРИМЕНЕНИЯ. Смотрите Стандартную Общественную Лицензию GNU для получения дополнительной информации.
Вы должны были получить копию Стандартной Общественной Лицензии GNU вместе с программой. В случае её отсутствия, посмотрите <http://www.gnu.org/licenses/gpl.html>.
*/

namespace Messages{
const char* const name[] = {
/*0*/	QT_TRANSLATE_NOOP("Print","diameter of a sphere:"),
/*1*/	QT_TRANSLATE_NOOP("Print","length of a pattern:"),
/*2*/	QT_TRANSLATE_NOOP("Print","pyramid corner:"),
/*3*/	QT_TRANSLATE_NOOP("Print","width of a pattern:"),
/*4*/	QT_TRANSLATE_NOOP("Print","diameter of an exit:"),
/*5*/	QT_TRANSLATE_NOOP("Print","the cover area:"),
/*6*/	QT_TRANSLATE_NOOP("Print","sphere volume:"),
/*7*/	QT_TRANSLATE_NOOP("Print","material density:"),
/*8*/	QT_TRANSLATE_NOOP("Print","air weight in a sphere:"),
/*9*/	QT_TRANSLATE_NOOP("Print","cover weight:"),
/*10*/	QT_TRANSLATE_NOOP("Montgolfier","File record"),
/*11*/	QT_TRANSLATE_NOOP("Print","SOLLAR BALL"),

		0};
const char* const size[] = {
/*0*/	QT_TRANSLATE_NOOP("Print"," cm"),
/*1*/	QT_TRANSLATE_NOOP("Print"," cm"),
/*2*/	QT_TRANSLATE_NOOP("Print",""),
/*3*/	QT_TRANSLATE_NOOP("Print"," cm"),
/*4*/	QT_TRANSLATE_NOOP("Print"," cm"),
/*5*/	QT_TRANSLATE_NOOP("Print"," m<sup>2</sup>"),
/*6*/	QT_TRANSLATE_NOOP("Print"," m<sup>3</sup>"),
/*7*/	QT_TRANSLATE_NOOP("Print"," g/m<sup>2</sup>"),
/*8*/	QT_TRANSLATE_NOOP("Print"," kg"),
/*9*/	QT_TRANSLATE_NOOP("Print"," kg"),
		0};
}
#endif // MESSAGES_H

