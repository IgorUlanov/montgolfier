#include "montgolfier.h"
#include <QPrintDialog>
#include "math.h"
#include "machine.h"
#include "print.h"
#include "messages.h"
#include "help.h"
#include <QDebug>
#include <QLocale>
#include <QSysInfo>
#include <QStandardPaths>
#include <QLayout>
#include <QFormLayout>
#include <QApplication>

Print print;
extern Machine machine;

Montgolfier::Montgolfier(QWidget *parent) :
	QMainWindow(parent)
{
	const char* language=QT_TR_NOOP("English");

	mainWidget = new QWidget();
	setCentralWidget(mainWidget);
    statusBar=new QStatusBar();
	setStatusBar(statusBar);
	statusBar->showMessage(tr("Ready"));
	QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	sizePolicy.setHorizontalStretch(0);
	sizePolicy.setVerticalStretch(0);
	setSizePolicy(sizePolicy);

	QHBoxLayout *horizontalLayout = new QHBoxLayout(mainWidget);
	widget = new Paint();
	sizePolicy.setHorizontalStretch(0);
	sizePolicy.setVerticalStretch(0);
	setSizePolicy(sizePolicy);
	widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	widget->setMinimumWidth(300);
	horizontalLayout->addWidget(widget);
	QVBoxLayout *verticalLayout = new QVBoxLayout();

	QHBoxLayout *horizontalLayout_10 = new QHBoxLayout();
	QFormLayout *formLayout_10 = new QFormLayout();
	formLayout_10->setHorizontalSpacing(20);
	formLayout_10->setVerticalSpacing(8);
    label_10 = new QLabel();
	label_10->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	formLayout_10->setWidget(0, QFormLayout::LabelRole, label_10);
	L_label = new QLabel();
	formLayout_10->setWidget(0, QFormLayout::FieldRole, L_label);
    label_11 = new QLabel();
	formLayout_10->setWidget(1, QFormLayout::LabelRole, label_11);
	W_label = new QLabel();
	formLayout_10->setWidget(1, QFormLayout::FieldRole, W_label);
    label_12 = new QLabel();
	formLayout_10->setWidget(2, QFormLayout::LabelRole, label_12);
	S_label = new QLabel();
	formLayout_10->setWidget(2, QFormLayout::FieldRole, S_label);
    label_13 = new QLabel();
	formLayout_10->setWidget(3, QFormLayout::LabelRole, label_13);
	V_label = new QLabel();
	formLayout_10->setWidget(3, QFormLayout::FieldRole, V_label);
    label_14 = new QLabel();
	formLayout_10->setWidget(4, QFormLayout::LabelRole, label_14);
	M_label = new QLabel();
    formLayout_10->setWidget(4, QFormLayout::FieldRole, M_label);
    label_15 = new QLabel();
	formLayout_10->setWidget(5, QFormLayout::LabelRole, label_15);
	M_air_label = new QLabel();
	formLayout_10->setWidget(5, QFormLayout::FieldRole, M_air_label);
    label_16 = new QLabel();
	formLayout_10->setWidget(6, QFormLayout::LabelRole, label_16);
	P_label = new QLabel();
	formLayout_10->setWidget(6, QFormLayout::FieldRole, P_label);
	QWidget *frame_10=new QWidget();
	frame_10->setObjectName("frame_10");
	frame_10->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	frame_10->setStyleSheet("QWidget#frame_10{border: 1px solid gray;border-radius: 4px}");
	frame_10->setLayout(formLayout_10);
	horizontalLayout_10->addWidget(frame_10);

	QHBoxLayout *horizontalLayout_20 = new QHBoxLayout();
	T_Slider = new QSlider();
	T_Slider->setMinimum(-40);
	T_Slider->setMaximum(40);
	T_Slider->setValue(20);
	T_Slider->setInvertedAppearance(false);
	T_Slider->setInvertedControls(false);
    T_Slider->setOrientation(Qt::Vertical);
    T_Slider->setTickPosition(QSlider::TicksBothSides);
	horizontalLayout_20->addWidget(T_Slider);
	QVBoxLayout *verticalLayout_20 = new QVBoxLayout();
	T_spinBox = new QSpinBox();
	T_spinBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	T_spinBox->setMinimum(-40);
	T_spinBox->setMaximum(40);
	T_spinBox->setValue(20);
	verticalLayout_20->addWidget(T_spinBox);
    label_20 = new QLabel();
	label_20->setWordWrap(true);
	label_20->setAlignment(Qt::AlignLeft);
	verticalLayout_20->addWidget(label_20);
    label_21 = new QLabel();
	label_21->setWordWrap(true);
	label_21->setAlignment(Qt::AlignRight);
	verticalLayout_20->addWidget(label_21);
	QHBoxLayout *horizontalLayout_21 = new QHBoxLayout();
	Delta_spinBox = new QSpinBox();
	Delta_spinBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	Delta_spinBox->setMinimum(1);
	Delta_spinBox->setMaximum(100);
	Delta_spinBox->setValue(10);
	horizontalLayout_21->addStretch();
	horizontalLayout_21->addWidget(Delta_spinBox);
	verticalLayout_20->addLayout(horizontalLayout_21);
	horizontalLayout_20->addLayout(verticalLayout_20);
	Delta_Slider = new QSlider();
	Delta_Slider->setMinimum(1);
	Delta_Slider->setMaximum(100);
	Delta_Slider->setValue(10);
	Delta_Slider->setOrientation(Qt::Vertical);
    Delta_Slider->setTickPosition(QSlider::TicksBothSides);
	horizontalLayout_20->addWidget(Delta_Slider);
	QWidget *frame_20=new QWidget();
	frame_20->setObjectName("frame_20");
	frame_20->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	frame_20->setStyleSheet("QWidget#frame_20{border: 1px solid gray;border-radius: 4px}");
	frame_20->setLayout(horizontalLayout_20);
	horizontalLayout_10->addWidget(frame_20);

	QVBoxLayout *verticalLayout_30 = new QVBoxLayout();
	QHBoxLayout *horizontalLayout_30 = new QHBoxLayout();
	horizontalLayout_30->addStretch();
    label_30 = new QLabel();
	horizontalLayout_30->addWidget(label_30);
	D_spinBox = new QSpinBox();
	D_spinBox->setMinimum(100);
	D_spinBox->setMaximum(1000);
	D_spinBox->setValue(200);
	horizontalLayout_30->addWidget(D_spinBox);
    label_31 = new QLabel();
	horizontalLayout_30->addWidget(label_31);
	horizontalLayout_30->addStretch();
	verticalLayout_30->addLayout(horizontalLayout_30);
	D_Slider = new QSlider();
	D_Slider->setMinimum(100);
	D_Slider->setMaximum(1000);
    D_Slider->setPageStep(25);
	D_Slider->setValue(200);
	D_Slider->setOrientation(Qt::Horizontal);
    D_Slider->setTickPosition(QSlider::TicksBelow);
	verticalLayout_30->addWidget(D_Slider);
	QHBoxLayout *horizontalLayout_31 = new QHBoxLayout();
	horizontalLayout_31->addStretch();
    label_32 = new QLabel();
	horizontalLayout_31->addWidget(label_32);
	Angle_spinBox = new QSpinBox();
	Angle_spinBox->setMinimum(30);
	Angle_spinBox->setMaximum(150);
	Angle_spinBox->setValue(60);
	horizontalLayout_31->addWidget(Angle_spinBox);
	QLabel *label_33 = new QLabel();
	horizontalLayout_31->addWidget(label_33);
	horizontalLayout_31->addStretch();
	verticalLayout_30->addLayout(horizontalLayout_31);
	Angle_Slider = new QSlider();
	Angle_Slider->setMinimum(30);
	Angle_Slider->setMaximum(150);
    Angle_Slider->setPageStep(5);
	Angle_Slider->setValue(60);
	Angle_Slider->setOrientation(Qt::Horizontal);
    Angle_Slider->setTickPosition(QSlider::TicksBelow);
	verticalLayout_30->addWidget(Angle_Slider);
	QHBoxLayout *horizontalLayout_32 = new QHBoxLayout();
	horizontalLayout_32->addStretch();
    label_34 = new QLabel();
	horizontalLayout_32->addWidget(label_34);
	D_out_spinBox = new QSpinBox();
	D_out_spinBox->setMinimum(1);
	D_out_spinBox->setMaximum(1000);
	D_out_spinBox->setValue(10);
	horizontalLayout_32->addWidget(D_out_spinBox);
    label_35 = new QLabel();
	horizontalLayout_32->addWidget(label_35);
	horizontalLayout_32->addStretch();
	verticalLayout_30->addLayout(horizontalLayout_32);
	D_out_Slider = new QSlider();
	D_out_Slider->setMinimum(1);
	D_out_Slider->setMaximum(1000);
    D_out_Slider->setPageStep(50);
	D_out_Slider->setValue(10);
	D_out_Slider->setOrientation(Qt::Horizontal);
    D_out_Slider->setTickPosition(QSlider::TicksBelow);
	verticalLayout_30->addWidget(D_out_Slider);

	QHBoxLayout *horizontalLayout_40 = new QHBoxLayout();
	QFormLayout *formLayout_40 = new QFormLayout();
    label_40 = new QLabel();
	label_40->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	formLayout_40->setWidget(0, QFormLayout::LabelRole, label_40);
	M_spinBox = new QSpinBox();
	M_spinBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
	M_spinBox->setMinimum(1);
	M_spinBox->setMaximum(200);
	M_spinBox->setValue(10);
	formLayout_40->setWidget(0, QFormLayout::FieldRole, M_spinBox);
    label_41 = new QLabel();
	label_41->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	formLayout_40->setWidget(1, QFormLayout::LabelRole, label_41);
	Step_spinBox = new QSpinBox();
	Step_spinBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
	Step_spinBox->setMinimum(10);
	Step_spinBox->setMaximum(200);
	Step_spinBox->setSingleStep(10);
	Step_spinBox->setValue(10);
	formLayout_40->setWidget(1, QFormLayout::FieldRole, Step_spinBox);
	horizontalLayout_40->addLayout(formLayout_40);
	QFormLayout *formLayout_41 = new QFormLayout();
    label_42 = new QLabel();
	label_42->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	formLayout_41->setWidget(0, QFormLayout::LabelRole, label_42);
	N_spinBox = new QSpinBox();
	N_spinBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
	N_spinBox->setMinimum(4);
	N_spinBox->setMaximum(64);
	N_spinBox->setValue(12);
	formLayout_41->setWidget(0, QFormLayout::FieldRole, N_spinBox);
    label_43 = new QLabel();
	label_43->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	formLayout_41->setWidget(1, QFormLayout::LabelRole, label_43);
	C_spinBox = new QSpinBox();
	C_spinBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
	C_spinBox->setMinimum(1);
	C_spinBox->setMaximum(1000);
	formLayout_41->setWidget(1, QFormLayout::FieldRole, C_spinBox);
	horizontalLayout_40->addLayout(formLayout_41);
	QWidget *frame_40=new QWidget();
	frame_40->setObjectName("frame_40");
	frame_40->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	frame_40->setStyleSheet("QWidget#frame_40{border: 1px solid gray;border-radius: 4px}");
	frame_40->setLayout(horizontalLayout_40);

	verticalLayout->addLayout(horizontalLayout_10);
    verticalLayout->addLayout(verticalLayout_30);
	verticalLayout->addWidget(frame_40);
	verticalLayout->addStretch();
	horizontalLayout->addLayout(verticalLayout);
	mainWidget->setLayout(horizontalLayout);

    txtAct = new QAction(this);
    connect(txtAct, SIGNAL(triggered()), this, SLOT(actionText()));
    csvAct = new QAction(this);
    connect(csvAct, SIGNAL(triggered()), this, SLOT(actionCSV()));
    pdfAct = new QAction(this);
    connect(pdfAct, SIGNAL(triggered()), this, SLOT(actionPDF()));
    helpAct = new QAction(this);
    connect(helpAct, SIGNAL(triggered()), this, SLOT(actionHelp()));
	// меню File
    saveAct = new QAction(this);
	connect(saveAct, SIGNAL(triggered()), this, SLOT(actionSave_As()));
    printAct = new QAction(this);
	connect(printAct, SIGNAL(triggered()), this, SLOT(actionPrint()));
    exitAct = new QAction(this);
	connect(exitAct, SIGNAL(triggered()), this, SLOT(actionExit()));
	menuBar = new QMenuBar();
	setMenuBar(menuBar);
    fileMenu=new QMenu();
    menuBar->addMenu(fileMenu);
	fileMenu->addAction(saveAct);
	fileMenu->addAction(printAct);
	fileMenu->addSeparator();
	fileMenu->addAction(exitAct);
    // toolbar
    toolBar=new QToolBar();
    addToolBar(toolBar);
    toolBar->addAction(printAct);
    toolBar->addAction(txtAct);
    toolBar->addAction(csvAct);
    toolBar->addAction(pdfAct);
    toolBar->addAction(helpAct);
    toolBar->addSeparator();
    toolBar->addAction(exitAct);
    // создание меню переключения языков
	newAct= new QAction(tr(language),this);
	newAct->setData(0);
	newAct->setStatusTip(QString(tr("Switching on %1 language").arg(tr(language))));
	connect(newAct, SIGNAL(triggered()), this, SLOT(changeLanguage()));
	menuLanguage = menuBar->addMenu(tr("&Language"));
	menuLanguage->addAction(newAct);
	QString locale = "en_EN";
	locales.push_back(locale);
	dir.setPath(":/my/lang/");
	QStringList fileNames = dir.entryList();
	for (int i = 0; i < (int)fileNames.size(); ++i){
	  apptr.load(fileNames[i],dir.path());
      QApplication::installTranslator(&apptr);
	  newAct= new QAction(tr(language),this);
	  newAct->setData(i+1);
	  newAct->setStatusTip(QString(tr("Switching on %1 language").arg(tr(language))));
	  connect(newAct, SIGNAL(triggered()), this, SLOT(changeLanguage()));
	  menuLanguage->addAction(newAct);
	  locales.push_back(fileNames[i]);
	}
    // меню Help
    programAct = new QAction(this);
    connect(programAct, SIGNAL(triggered()), this, SLOT(actionProgram()));
    licenseAct = new QAction(this);
    connect(licenseAct, SIGNAL(triggered()), this, SLOT(actionLicense()));
    aboutAct = new QAction(this);
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(actionAbout()));
    helpMenu=new QMenu();
    menuBar->addMenu(helpMenu);
    helpMenu->addAction(programAct);
    helpMenu->addAction(licenseAct);
    helpMenu->addAction(aboutAct);
    apptr.load(QLocale::system().name(),dir.path()); // загрузка файла qt_*.qm
    QApplication::installTranslator(&apptr); // установка переводчика

	printer=new QPrinter;
	connect(D_Slider,SIGNAL(valueChanged(int)),this,SLOT(Change_D(int)));
	connect(Angle_Slider,SIGNAL(valueChanged(int)),this,SLOT(Change_Angle(int)));
	connect(D_out_Slider,SIGNAL(valueChanged(int)),this,SLOT(Change_D_out(int)));
    connect(D_spinBox,SIGNAL(valueChanged(int)),this,SLOT(D_spinBox_changed(int)));
    connect(Angle_spinBox,SIGNAL(valueChanged(int)),this,SLOT(Angle_spinBox_changed(int)));
    connect(D_out_spinBox,SIGNAL(valueChanged(int)),this,SLOT(D_out_spinBox_changed(int)));
    connect(N_spinBox,SIGNAL(valueChanged(int)),this,SLOT(Change_N(int)));
	connect(M_spinBox,SIGNAL(valueChanged(int)),this,SLOT(Change_M(int)));
	connect(Step_spinBox,SIGNAL(valueChanged(int)),this,SLOT(Change_Step()));
    connect(T_Slider,SIGNAL(valueChanged(int)),this,SLOT(Change_T()));
    connect(Delta_Slider,SIGNAL(valueChanged(int)),this,SLOT(Change_T()));
    connect(T_spinBox,SIGNAL(valueChanged(int)),this,SLOT(T_spinBox_changed(int)));
    connect(Delta_spinBox,SIGNAL(valueChanged(int)),this,SLOT(Delta_spinBox_changed(int)));

    machine.Set_Angle(Angle_Slider->value());
	machine.Set_D_out(D_out_Slider->value());
	machine.Set_N(N_spinBox->value());
	machine.Set_M(M_spinBox->value());
	machine.Change_D(D_Slider->value());
	machine.Change_T(T_Slider->value(),Delta_Slider->value());
	Limit(); // ограничение максимального диаметра выходного отверстия
	Refresh_all();
	path_save=QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first();
}

Montgolfier::~Montgolfier(){
	delete printer;
}

void Montgolfier::changeEvent(QEvent *e){
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        txtAct->setIcon(QIcon(":/my/icons/txt_24.png"));
        txtAct->setText("Save as format TXT");
        txtAct->setStatusTip(tr("Save as format TXT"));
        csvAct->setIcon(QIcon(":/my/icons/csv_24.png"));
        csvAct->setText(tr("Save as format CSV"));
        csvAct->setStatusTip(tr("Save as format CSV"));
        pdfAct->setIcon(QIcon(":/my/icons/acrobat_24.png"));
        pdfAct->setText(tr("Save as format PDF"));
        pdfAct->setStatusTip(tr("Save as format PDF"));
        helpAct->setIcon(QIcon(":/my/icons/help_24.png"));
        helpAct->setText(tr("Help"));
        helpAct->setStatusTip(tr("Help"));
        saveAct->setIcon(QIcon(":/my/icons/1519.bmp"));
        saveAct->setText(tr("&Save as ..."));
        saveAct->setStatusTip(tr("Save as ..."));
        printAct->setIcon(QIcon(":/my/icons/print_24.png"));
        printAct->setText(tr("&Print"));
        printAct->setStatusTip(tr("Print"));
        exitAct->setIcon(QIcon(":/my/icons/exit_24.png"));
        exitAct->setText(tr("&Exit"));
        exitAct->setStatusTip(tr("Exit"));
        programAct->setText(tr("Description"));
        programAct->setStatusTip(tr("Description"));
        licenseAct->setText(tr("License"));
        licenseAct->setStatusTip(tr("License"));
        aboutAct->setText(tr("About"));
        aboutAct->setStatusTip(tr("About"));
        label_10->setText(tr("length of a pattern (cm):"));
        label_11->setText(tr("width of a pattern (cm):"));
        label_12->setText(tr("the cover area (m<sup>2</sup>):"));
        label_13->setText(tr("sphere volume (m<sup>3</sup>):"));
        label_14->setText(tr("cover weight:"));
        label_15->setText(tr("air weight in a ball (kg):"));
        label_16->setText(tr("carrying power (kg):"));
        label_20->setText(QString(tr("air temperature%1<<<")).arg("\n"));
        label_21->setText(QString(tr(">>>%1difference of%1temperatures")).arg("\n"));
        label_30->setText(tr("diameter of a ball:"));
        label_31->setText(tr("cm"));
        label_32->setText(tr("pyramid corner:"));
        label_34->setText(tr("diameter of an exit:"));
        label_35->setText(tr("cm"));
        label_40->setText(tr("material density (g/m<sup>2</sup>):"));
        label_41->setText(tr("step of points (cm):"));
        label_42->setText(tr("ball gores:"));
        label_43->setText(tr("count of points:"));
        helpMenu->setTitle(tr("&Help"));
        menuLanguage->setTitle(tr("&Language"));
        fileMenu->setTitle(tr("&File"));
        Refresh_all();
        break;
    default:
        break;
    }
}

void Montgolfier::Change_D(int d){
    if (d!=D_spinBox->value()) D_spinBox->setValue(d);
	machine.Change_D(d);
	widget->repaint();
	Limit(); // ограничение максимального диаметра выходного отверстия
	Refresh_all();
}

void Montgolfier::Change_Angle(int d){
    if (d!=Angle_spinBox->value()) Angle_spinBox->setValue(d);
	machine.Change_Angle(d);
	widget->repaint();
	Limit(); // ограничение максимального диаметра выходного отверстия
	Refresh_all();
}

void Montgolfier::Change_D_out(int d){
    if (d!=D_out_spinBox->value()) D_out_spinBox->setValue(d);
    machine.Change_D_out(d);
	widget->repaint();
	Refresh_all();
}

void Montgolfier::Change_N(int d)
{
	machine.Change_N(d);
	widget->repaint();
	W_label->setText(QString("%1 cm").arg(machine.Get_w(),0,'g',4));
}

void Montgolfier::Change_T(void){
    if (T_Slider->value()!=T_spinBox->value()) T_spinBox->setValue(T_Slider->value());
    if (Delta_Slider->value()!=Delta_spinBox->value()) Delta_spinBox->setValue(Delta_Slider->value());
    machine.Change_T(T_Slider->value(),Delta_Slider->value());
	Refresh_t();
}

void Montgolfier::Change_M(int d)
{ // изменение веса материала
	machine.Change_M(d);
	Refresh_t();
}

void Montgolfier::Refresh_all(void)
{ // обновление полей с параметрами
    L_label->setText(QString(tr("%1 cm")).arg(machine.Get_L()*100,0,'g',4));
    W_label->setText(QString(tr("%1 cm")).arg(machine.Get_w(),0,'g',4));
    S_label->setText(QString(tr("%1 m<sup>2</sup>")).arg(machine.Get_S(),0,'g',4));
    V_label->setText(QString(tr("%1 m<sup>3</sup>")).arg(machine.Get_V(),0,'g',3));
	Limit_step();
	Change_Step();
	Refresh_t();
}

void Montgolfier::Refresh_t(void)
{ // обновление полей с параметрами
    M_label->setText(QString(tr("%1 kg")).arg(ceil(machine.Get_M()*1000)/1000,0,'g',3));
    M_air_label->setText(QString(tr("%1 kg")).arg(machine.Get_M_air(),0,'g',3));
    P_label->setText(QString(tr("%1 kg")).arg(ceil(machine.Get_P()*1000)/1000,0,'g',3));
}

void Montgolfier::Limit(void)
{ // ограничение максимального диаметра выходного отверстия
	D_out_Slider->setMaximum(machine.Get_limit());
	D_out_spinBox->setMaximum(machine.Get_limit());
}

void Montgolfier::Change_Step(void)
{
	C_spinBox->setValue((int)ceil((machine.Get_L()*100)/Step_spinBox->value())+1);
}

void Montgolfier::Limit_step(void)
{ // определение максимального и минимального значения шага точек отсчета
	Step_spinBox->setMinimum((int)ceil((machine.Get_L()*10)/count_step)*10);
	if (machine.Get_L()<2.0) Step_spinBox->setMaximum((int)floor(machine.Get_L()*10)*10);
		else Step_spinBox->setMaximum(200);
}

void Montgolfier::actionPrint()
{ // вывод таблицы со значениями выкройки на печать
	printer->setOutputFileName("");
	QPrintDialog *dialog = new QPrintDialog(printer, this);
	if (dialog->exec() == QDialog::Accepted)
	{
		statusBar->showMessage(tr("File printing"),10000);
		printer->setPageMargins(15,15,15,15,QPrinter::Millimeter);
		print.Draw(printer,Step_spinBox->value());
	}
}

void Montgolfier::actionExit(){
	close();
}


void Montgolfier::actionSave_As()
{
	// запись результата в файл
    dialog = new QFileDialog( this, "file dialog" );
    QStringList filters;
    filters << "PDF file (*.pdf)"
            << "Text file (*.txt)"
            << "CSV file (*.csv)";
    QString file_name=QString("d%1_a%2_o%3")
		.arg(D_Slider->value())
		.arg(Angle_Slider->value())
		.arg(D_out_Slider->value());
    dialog->setAcceptMode(QFileDialog::AcceptSave);
    dialog->setFileMode(QFileDialog::AnyFile);
    dialog->setDirectory(path_save);
    dialog->setNameFilters(filters);
    dialog->selectFile(file_name);
    if (dialog->exec() == QDialog::Accepted)
    {
    QString file_exec=dialog->selectedNameFilter();
    QStringList file_name=dialog->selectedFiles();
    if (file_exec=="PDF file (*.pdf)")
        {
        // запись файла в формате PDF
        file_exec="pdf";
        printer->setOutputFileName(file_name[0]+".pdf");
		print.Draw(printer,Step_spinBox->value());
        }
    else
        {
        if (file_exec=="Text file (*.txt)") file_exec="txt"; else file_exec="csv";
		print.Text(Step_spinBox->value(),file_name[0]+"."+file_exec,file_exec); // запись файла в форматe csv или формате txt
        }
	statusBar->showMessage(tr("File recording")+":  "+QDir::toNativeSeparators(file_name[0]+"."+file_exec),10000);
    }
    delete dialog;
}

void Montgolfier::changeLanguage(void)
{ // переключение языков
	newAct=(QAction *)sender();
	apptr.load(locales[newAct->data().toInt()],dir.path());
	QApplication::installTranslator(&apptr);
}

void Montgolfier::actionText()
{ // запись результата в файл с расширением txt
	QString name_file=path_save+"/"+QString("d%1_a%2_o%3.txt")
				.arg(D_Slider->value())
				.arg(Angle_Slider->value())
				.arg(D_out_Slider->value());
		statusBar->showMessage(tr("File recording")+":  "+QDir::toNativeSeparators(name_file),10000);
	print.Text(Step_spinBox->value(),name_file,"txt");
}

void Montgolfier::actionCSV()
{ // запись результата в файл с расширением csv
		QString name_file=path_save+"/"+QString("d%1_a%2_o%3.csv")
				.arg(D_Slider->value())
				.arg(Angle_Slider->value())
				.arg(D_out_Slider->value());
	statusBar->showMessage(tr("File recording")+":  "+QDir::toNativeSeparators(name_file),10000);
	print.Text(Step_spinBox->value(),name_file,"csv");
}

void Montgolfier::actionPDF()
{ // запись файла в формате PDF
		QString name_file=path_save+"/"+QString("d%1_a%2_o%3.pdf")
				.arg(D_Slider->value())
				.arg(Angle_Slider->value())
				.arg(D_out_Slider->value());
	statusBar->showMessage(tr("File recording")+":  "+QDir::toNativeSeparators(name_file),10000);
	printer->setOutputFileName(name_file);
	print.Draw(printer,Step_spinBox->value());
}

void Montgolfier::actionHelp(){
	// Вызов общей справки
	HelpBrowser::showPage("help.html",800,550);
}

void Montgolfier::actionProgram(){
    // Вызов описания программы
    HelpBrowser::showPage("program.html",800,550);
}

void Montgolfier::actionLicense(){
    // Вызов лицензии
    HelpBrowser::showPage("license.html",600,300);
}

void Montgolfier::actionAbout(){
    // Вызов информации об авторе
    HelpBrowser::showPage("about.html",400,200);
}

void Montgolfier::Delta_spinBox_changed(int value){
    if (value!=Delta_Slider->value()) Delta_Slider->setValue(value);
}

void Montgolfier::T_spinBox_changed(int value){
    if (value!=T_Slider->value()) T_Slider->setValue(value);
}

void Montgolfier::D_spinBox_changed(int value){
    if (value!=D_Slider->value()) D_Slider->setValue(value);
}

void Montgolfier::Angle_spinBox_changed(int value){
    if (value!=Angle_Slider->value()) Angle_Slider->setValue(value);
}

void Montgolfier::D_out_spinBox_changed(int value){
    if (value!=D_out_Slider->value()) D_out_Slider->setValue(value);
}
